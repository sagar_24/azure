terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "gitops-talks"

    workspaces {
      name = "azure"
    }
  }
}